// db.users.insertOne({

//     "username":"dahyunTwice",
//     "password":"dahyunKim"

// })

// db.users.insertOne({

//     "username":"gokusSon",
//     "password":"over9000"

// })



// Insert Mutiple documents at once

// db.users.insertMany(

//     [
//         {
//             "username":"pablo123",
//             "password":"123paul"
//         },
//         {
//             "username":"pedro99",
//             "password":"iampeter99"
//         }
//     ]

// )

// db.products.insertMany(
//     [
//         {
//             "name":"Strawberry Smoothie",
//             "description":"Sweet Strawberry",
//             "price": 200
//         },
//         {
//             "name":"Mango Smoothie",
//             "description":"Yummy Mango",
//             "price": 100
//         },
//         {
//             "name":"Ampalaya Smoothie",
//             "description":"Eww!",
//             "price": 20
//         }
//     ]
// )

// db.collection.find() - return/find all documents in the collection.
// db.users.find()

// db.users.find({
//     "username":"pedro99"
// })

// db.cars.insertMany(
//     [
//         {
//             "name":"Vios",
//             "brand":"Toyota",
//             "type":"Sedan",
//             "price":1500000
//         },
//         {
//             "name":"Tamaraw FX",
//             "brand":"Toyota",
//             "type":"AUV",
//             "price":700000
//         },
//         {
//             "name":"City",
//             "brand":"Honda",
//             "type":"Sedan",
//             "price":1600000
//         }
//     ]
// )

// db.cars.find({
//     "type":"Sedan"
// })

// db.cars.find({
//     "brand":"Toyota"
// })

// db.collection.findOne() - find/return the first item/document in the collection.
// db.cars.findOne({
//     "brand":"Toyota"
// })

// db.cars.find({"brand":"Honda"})



// UPDATE

// db.collection.findOne({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})

// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter1999"}})



// Will update the first item in the collection

// db.users.updateOne({},{$set:{"username":"UpdatedUsername"}})



// db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin":true}})



// This will allow us to update all items in the collection.

// db.users.updateMany({},{$set:{"isAdmin":true}})



// Allows us to update all items in the collection that matches our criteria

// db.cars.updateMany({"type":"Sedan"},{$set:{"price":1000000}})



// DELETE

// Delete the first item in the collection

// db.products.deleteOne({})





// Delete the first item that matches our criteria

// db.cars.deleteOne({"brand":"Toyota"})



// Delete all items in the collection that matches our criteria

// db.users.deleteMany({"isAdmin":true})



// db.collection.deleteMany()

// delete all documents in a collection



// db.products.deleteMany({})

// db.cars.deleteMany({})